package info.androidhive.slidingmenu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectOutputStream.PutField;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import BitalinoExternal.BITalinoDevice;
import BitalinoExternal.BITalinoFrame;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;



public class HomeFragment extends Fragment {
	
	private static final String TAG = "test";
	private boolean testInitiated = false;
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	private GraphicalView mChart;

    private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();

    private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();

    private XYSeries mCurrentSeries;

    private XYSeriesRenderer mCurrentRenderer;
    
    FileOutputStream outStream = null;
    
    TestAsyncTask testAyc = new TestAsyncTask();

    private int index_x = 0;
    private int poslji = 1;
    
    ProgressDialog dialog;
    
    private BluetoothSocket sock = null;
    
    File yourFile = null;
    
    PrintStream printStream = null;
    BITalinoDevice bitalino = null;
    
	private void initChart() {
        mCurrentSeries = new XYSeries("Sample Data");
        mDataset.addSeries(mCurrentSeries);
        mCurrentRenderer = new XYSeriesRenderer();
        mRenderer.addSeriesRenderer(mCurrentRenderer);
        mRenderer.setYAxisMax(900);
        mRenderer.setYAxisMin(200);
        mRenderer.setApplyBackgroundColor(true);
        mRenderer.setBackgroundColor(Color.BLACK);
        mCurrentRenderer.setColor(Color.RED);
        mCurrentRenderer.setLineWidth(2);

    }
	
	public HomeFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
		try{
		
        Log.d("test", "started");
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                Log.d(TAG, device.getName() + "\n" + device.getAddress());
            }
        }
        
        	String filePath = getActivity().getFilesDir().getPath().toString() + "/sensory_data.txt";
            yourFile = new File(filePath);
            if (!yourFile.exists()) {
                yourFile.createNewFile();
            }
            outStream = new FileOutputStream(yourFile, false);
            printStream = new PrintStream(outStream);
            byte dataToWrite[] = null;
        }catch(Exception e){
        	
        }

        if (!testInitiated)
            testAyc.execute();
        
        return rootView;
    }
	
	@Override
	public void onResume() {
		super.onResume();
        LinearLayout layout = (LinearLayout)getView().findViewById(R.id.chart);
        if (mChart == null) {
            initChart();
            mChart = ChartFactory.getCubeLineChartView(getActivity(), mDataset, mRenderer, 0.3f);
            layout.addView(mChart);
        } else {
            mChart.repaint();
        }
        
	}
	
	
	private class TestAsyncTask extends AsyncTask<Void, BITalinoFrame[], Void> {
		private BluetoothDevice dev = null;
        
		@Override
	    protected void onPreExecute() {
			dialog = ProgressDialog.show(getActivity(), "Connecting...", "Please wait...", true);
	    }
		
        @Override
        protected Void doInBackground(Void... paramses) {
            //FileOutputStream outStream = null;
            

            try {
                // Let's get the remote Bluetooth device
                final String remoteDevice = "98:D3:31:B1:83:38";

                final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                dev = btAdapter.getRemoteDevice(remoteDevice);

                Log.d(TAG, "Stopping Bluetooth discovery.");
                btAdapter.cancelDiscovery();
                
                sock = null;
                sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
                sock.connect();
                testInitiated = true;

                bitalino = new BITalinoDevice(1000, new int[]{0,1,2,3});
                Log.d(TAG, "Connecting to BITalino [" + remoteDevice + "]..");
                bitalino.open(sock.getInputStream(), sock.getOutputStream());
                Log.d(TAG, "Connected.");

                // get BITalino version
                Log.d(TAG, "Version: " + bitalino.version());

                // start acquisition on predefined analog channels
                bitalino.start();
                int i = 0;
                while (true) {
                    // read n samples
                    final int numberOfSamplesToRead = 2000;
                    BITalinoFrame[] frames = bitalino.read(numberOfSamplesToRead);
                    publishProgress(frames);
                    i++;
                    //publishProgress(yourFile);
                }

            } catch (Exception e) {
            	try {
	            	bitalino.stop();
	                sock.close();
            	} catch (Exception e1) {
                    e1.printStackTrace();
                }
            } finally {
                try {
                    // trigger digital outputs
                    // int[] digital = { 1, 1, 1, 1 };
                    // device.trigger(digital);
                    bitalino.stop();
                    sock.close();
                    // stop acquisition and close bluetooth connection



                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
        @Override
        protected void onProgressUpdate(BITalinoFrame[]... values) {
            Log.d(TAG, "Publish");
            if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
            
            try {
                int i = 0;
                for (BITalinoFrame frame : values[0]) {
                    printStream.print(Integer.toString(frame.getAnalog(2)) + ";" + frame.getAnalog(1) + "\n");
                    i++;
                    if (i%20==0){
                        //Log.d(TAG, "Risem");
                        mCurrentSeries.add(index_x,frame.getAnalog(2));
                        if(index_x>20000)
                            mCurrentSeries.remove(0);
                        index_x+=6;

                        mChart.repaint();

                    }
                }
                mRenderer.setXAxisMax(index_x);
                mRenderer.setXAxisMin(index_x-2000);
                
                if (poslji%30==0){
                	Log.d(TAG, "POSILJAM");
                	try {
                		BufferedReader br = new BufferedReader(new FileReader(yourFile));
                        StringBuilder sb = new StringBuilder();
                        try {
                            String line = br.readLine();

                            while (line != null) {
                                sb.append(line);
                                sb.append("\n");
                                line = br.readLine();
                            }

                        } finally {
                            br.close();
                        }                        
                        
	                	/*Time now = new Time();
	                    now.setToNow();
	                    //LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	                    //Criteria criteria = new Criteria();
	                    //String provider = locationManager.getBestProvider(criteria, false);
	                    //Location location = locationManager.getLastKnownLocation(provider);
	
	                    HttpClient httpClient = new DefaultHttpClient();
	                    HttpPost httpPost = new HttpPost("http://88.200.24.2:8000/hesudi/");
	
	                    List<NameValuePair> params = new ArrayList<NameValuePair>();
	                    params.add(new BasicNameValuePair("data", sb.toString()));
	                    params.add(new BasicNameValuePair("long", "10"));
	                    //Log.d(TAG, Double.toString(location.getLongitude()));
	                    params.add(new BasicNameValuePair("lat", "12"));
	                    //Log.d(TAG, Double.toString(location.getLatitude()));
	                    String time = Integer.toString(now.year) + "-" + Integer.toString(now.month) + "-"+Integer.toString(now.monthDay)+"T"+Integer.toString(now.hour)+":"+Integer.toString(now.minute);
	                    params.add(new BasicNameValuePair("time", time));
	                    Log.d(TAG, time);
	                    httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
	                    HttpResponse response = httpClient.execute(httpPost);
	                    HttpEntity respEntity = response.getEntity();
	
	                    if (respEntity != null) {
	                        // EntityUtils to get the response content
	                        String content =  EntityUtils.toString(respEntity);
	                        Log.d(TAG, "Response: "+content);
	                    }*/
                	}catch(Exception e){
                		e.printStackTrace();
                	}
                }
                //mRenderer.setZoomEnabled(false, false);
                //mRenderer.setClickEnabled(false);

                //outStream.close();
                poslji++;
            }catch (Exception e){

            }
        }
	}
	
	@Override
	public void onStop() {
		try{
			super.onStop();
			Log.d(TAG, "Response: ");
			testAyc.cancel(true);
			bitalino.stop();
	        sock.close();
		}catch(Exception e){
			Log.d(TAG, "Except onStop");
			//Log.d(TAG, "Except onStop");
		}
		
	}
	
	@Override
	public void onStart() {
		try{
			//testAyc.execute();
		}catch(Exception e){
			Log.d(TAG, "Except onStart");
		}
		
		super.onStart();
	}
}
