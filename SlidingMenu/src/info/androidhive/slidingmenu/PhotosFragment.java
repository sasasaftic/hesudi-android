package info.androidhive.slidingmenu;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PhotosFragment extends Fragment {
	ProgressDialog dialog;
	public PhotosFragment(){}
	ListView listView;
    List<RowItem> rowItems;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_photos, container, false);
         
        new TestAsyncTask().execute();
        return rootView;
    }
	
	private class TestAsyncTask extends AsyncTask<Void, String, String> {
		@Override
	    protected void onPreExecute() {
			dialog = ProgressDialog.show(getActivity(), "Loading...", "Please wait...", true);
	    }
		
		@Override
		protected String doInBackground(Void... params) {
			return "test";
		}
		@Override
		protected void onProgressUpdate(String... values) {
			
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(String result) {
			RowItem item;
			try{
				if (dialog.isShowing()) {
		            dialog.dismiss();
		        }
				ListView listView = (ListView) getView().findViewById(R.id.list2);
				rowItems = new ArrayList<RowItem>();
				//String[] responses = new String[result.length()];
				Double lon;
	            item = new RowItem(R.drawable.emoticon_grin, "Home", "Average heart rate: 67.");
	            rowItems.add(item);
	            item = new RowItem(R.drawable.emoticon_smile, "Work", "Average heart rate: 82.");
	            rowItems.add(item);
	            item = new RowItem(R.drawable.emoticon_sleeping, "Traveling", "Average heart rate: 60.");
	            rowItems.add(item);

	            CustomBaseAdapter adapter = new CustomBaseAdapter(getActivity(), rowItems);
	            listView.setAdapter(adapter); 
				}catch(Exception e){
		        	e.printStackTrace();
		        }
			super.onPostExecute(result);
		}
	}
}

