package info.androidhive.slidingmenu;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import BitalinoExternal.BITalinoFrame;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class FindPeopleFragment extends Fragment {
	ProgressDialog dialog;
	
	ListView listView;
    List<RowItem> rowItems;
	
	public FindPeopleFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_find_people, container, false);
		new TestAsyncTask().execute();
        return rootView;
    }
	
	private class TestAsyncTask extends AsyncTask<Void, String, JSONArray> {
		@Override
	    protected void onPreExecute() {
			dialog = ProgressDialog.show(getActivity(), "Loading...", "Please wait...", true);
	    }
		
		@Override
		protected JSONArray doInBackground(Void... params) {
			try{
		        
		        HttpClient httpClient = new DefaultHttpClient();
		        HttpGet httpGet = new HttpGet("http://88.200.24.2:8000/api/v1/data/analyses/");
		        //HttpGet httpGet = new HttpGet("http://192.168.43.154:8000/api/v1/data/analyses/");
		
		        HttpResponse response = httpClient.execute(httpGet);
		        HttpEntity respEntity = response.getEntity();
		
		        if (respEntity != null) {
		            String retSrc = EntityUtils.toString(respEntity); 
		            //retSrc = retSrc.replace("[", "");
		            //retSrc = retSrc.replace("]", "");
		            // parsing JSON
		            
		            JSONArray arr = new JSONArray(retSrc);
		            return arr;
		         }
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			return null;
		}
		@Override
		protected void onProgressUpdate(String... values) {
			
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(JSONArray result) {
			RowItem item;
			try{
				if (dialog.isShowing()) {
		            dialog.dismiss();
		        }
				ListView listView = (ListView) getView().findViewById(R.id.list);
				rowItems = new ArrayList<RowItem>();
				//String[] responses = new String[result.length()];
				Double lon;
	            for(int i = 0; i < result.length(); i++){
	            	
	            	try{
	            		lon = Double.parseDouble(result.getJSONObject(i).getString("lon"));
	            	}catch(Exception e){
	            		lon = 10.1;
	            	}
	            	
	            	if (lon > 10 && lon < 11){
	            		item = new RowItem(R.drawable.home, "Home", "From: " + result.getJSONObject(i).getString("time_from") + " To: " + result.getJSONObject(i).getString("time_to") + "Heart rate: "+result.getJSONObject(i).getString("avg_heartbeat") + " Excitement: "+result.getJSONObject(i).getString("avg_excitement") + " Feeling: "+ result.getJSONObject(i).getString("general_feeling"));
	            		rowItems.add(item);
	            	}else if(lon > 11 && lon < 12){
	            		item = new RowItem(R.drawable.money_bag, "Work", "From: " + result.getJSONObject(i).getString("time_from") + " To: " + result.getJSONObject(i).getString("time_to") + "Heart rate: "+result.getJSONObject(i).getString("avg_heartbeat") + " Excitement: "+result.getJSONObject(i).getString("avg_excitement") + " Feeling: "+ result.getJSONObject(i).getString("general_feeling"));
	            		rowItems.add(item);
	            	}else{
	            		item = new RowItem(R.drawable.world, "Traveling", "From: " + result.getJSONObject(i).getString("time_from") + " To: " + result.getJSONObject(i).getString("time_to") + "Heart rate: "+result.getJSONObject(i).getString("avg_heartbeat") + " Excitement: "+result.getJSONObject(i).getString("avg_excitement") + " Feeling: "+ result.getJSONObject(i).getString("general_feeling"));
	            		rowItems.add(item);
	            	}
	            		
	                //Log.d("test", result.getJSONObject(i).toString());
	                //responses[i]= "From: " + result.getJSONObject(i).getString("time_from") + " To: " + result.getJSONObject(i).getString("time_to") + "Heart rate: "+result.getJSONObject(i).getString("avg_heartbeat") + " Excitement: "+result.getJSONObject(i).getString("avg_excitement") + " Feeling: "+ result.getJSONObject(i).getString("general_feeling");
	            }
	            CustomBaseAdapter adapter = new CustomBaseAdapter(getActivity(), rowItems);
	            listView.setAdapter(adapter); 
				}catch(Exception e){
		        	e.printStackTrace();
		        }
			super.onPostExecute(result);
		}
	}
}
